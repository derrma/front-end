jQuery(document).ready(function($){
	//if you change this breakpoint in the style.css file (or _layout.scss if you use SASS), don't forget to update this value as well
	var $L = 1200,
		$menu_navigation = $('#main-nav'),
		$cart_trigger = $('.cd-cart-trigger'),
		$hamburger_icon = $('#cd-hamburger-menu'),
		$lateral_cart = $('#cd-cart'),
		$shadow_layer = $('#cd-shadow-layer');

	//open lateral menu on mobile
	$hamburger_icon.on('click', function(event){
		event.preventDefault();
		//close cart panel (if it's open)
		$lateral_cart.removeClass('speed-in');
		toggle_panel_visibility($menu_navigation, $shadow_layer, $('body'));
	});

	//open cart
	$cart_trigger.on('click', function(event){
		event.preventDefault();
		//close lateral menu (if it's open)
		$menu_navigation.removeClass('speed-in');
		toggle_panel_visibility($lateral_cart, $shadow_layer, $('body'));
	});

	//close lateral cart or lateral menu
	$shadow_layer.on('click', function(){
		$shadow_layer.removeClass('is-visible');
		// firefox transitions break when parent overflow is changed, so we need to wait for the end of the trasition to give the body an overflow hidden
		if( $lateral_cart.hasClass('speed-in') ) {
			$lateral_cart.removeClass('speed-in').on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
			$('body').removeClass('overflow-hidden');
			});
			$menu_navigation.removeClass('speed-in');
		} else {
			$menu_navigation.removeClass('speed-in').on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
				$('body').removeClass('overflow-hidden');
			});
			$lateral_cart.removeClass('speed-in');
		}
	});
});

	// Get Scrollbar Width
	function getScrollbarWidth() {
	  	var outer = document.createElement("div");
	  	outer.style.visibility = "hidden";
	  	outer.style.width = "100px";
	  	outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps

	  	document.body.appendChild(outer);

	  	var widthNoScroll = outer.offsetWidth;
	  	// force scrollbars
	  	outer.style.overflow = "scroll";

	  	// add innerdiv
	  	var inner = document.createElement("div");
	  	inner.style.width = "100%";
	  	outer.appendChild(inner);        

	  	var widthWithScroll = inner.offsetWidth;

	  	// remove divs
	  	outer.parentNode.removeChild(outer);
	  	return widthNoScroll - widthWithScroll;
	}

	function toggle_panel_visibility ($lateral_cart, $background_layer, $body, $cart_items) {
		if( $lateral_cart.hasClass('speed-in') ) {
			// firefox transitions break when parent overflow is changed, so we need to wait for the end of the trasition to give the body an overflow hidden
			$lateral_cart.removeClass('speed-in').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
			$('.navbar').removeAttr('style');
			$body.addClass('overflow-auto');
			});
			$body.removeAttr('style');
			$('.navbar').removeAttr('style');
			$body.removeClass('overflow-hidden');
			$background_layer.removeClass('is-visible');

		} else {
			$lateral_cart.addClass('speed-in').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
			$body.removeClass('overflow-auto');
			$body.addClass('overflow-hidden');
			$('.navbar').css({width: 'calc(100% - '+ getScrollbarWidth() +'px)'});
			$body.css({width: 'calc(100% - '+ getScrollbarWidth() +'px)'});
			$('.cd-cart-items-wrap').addClass('cart-overflow');
			$('.cd-cart-header').css({width: 'calc(100% - '+ getScrollbarWidth() +'px)'});
			$('.cd-cart-footer').css({width: 'calc(100% - '+ getScrollbarWidth() +'px)'});
			});
			$('.cd-search').removeClass('is-visible');
			$('.cd-search-trigger').removeClass('search-is-visible');
			$('#cd-shadow-layer').removeClass('search-is-visible');
			$background_layer.addClass('is-visible');
			$lateral_cart.css('overflow-y', '');
			$cart_items.css('overflow-y', 'scroll')
		}
}