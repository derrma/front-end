// Create a <select> and append to #menu
var $select = $('<select></select>');
$('#setting-sidebar').append($select);

// Cycle over menu links
$('#setting-sidebar a').each(function(){
  var $anchor = $(this);
  
  // Create an option
  var $option = $('<option></option>');

  // Deal with selected options depending on current page
  if($anchor.parent().hasClass('selected')) {
    $option.prop('selected', true);
  }

  // Option's value is the href
  $option.val($anchor.attr('href'));

  // Option's text is the text of link
  $option.text($anchor.text());

  // Append option to <select>
  $select.append($option);

});

$select.change(function(){
  window.location = $select.val();
});
