jQuery(document).ready(function($){
	//Big Headline Magic
	$(".page-title-large").fitText(1, { minFontSize: '40px', maxFontSize: '75px' });
    $(".page-title-medium").fitText(1, { minFontSize: '36px', maxFontSize: '64px' });
    $(".page-title-small").fitText(1, { minFontSize: '24px', maxFontSize: '50px' });
    $(".terima-kasih-large").fitText(1.1, { minFontSize: '30px', maxFontSize: '75px' });
	//if you change this breakpoint in the style.css file (or _layout.scss if you use SASS), don't forget to update this value as well
	var MqL = 1170;
	//move nav element position according to window width
	moveNavigation();
	$(window).on('resize', function(){
		(!window.requestAnimationFrame) ? setTimeout(moveNavigation, 300) : window.requestAnimationFrame(moveNavigation);
	});

	//mobile - open lateral menu clicking on the menu icon
	$('.cd-nav-trigger').on('click', function(event){
		event.preventDefault();
		if( $('.cd-main-content').hasClass('nav-is-visible') ) {
			closeNav();
			$('#cd-shadow-layer').removeClass('is-visible');
		} else {
			$(this).addClass('nav-is-visible');
			$('.cd-primary-nav').addClass('nav-is-visible');
			$('.cd-main-header').addClass('nav-is-visible');
			$('.cd-main-content').addClass('nav-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
				$('body').addClass('overflow-hidden');
			});
			toggleSearch('close');
			$('#cd-shadow-layer').addClass('is-visible');
		}
	});

	//open search form
	$('.cd-search-trigger').on('click', function(event){
		event.preventDefault();
		toggleSearch();
		closeNav();
	});

	//close lateral menu on mobile 
	$('#cd-shadow-layer').on('swiperight', function(){
		if($('.cd-primary-nav').hasClass('nav-is-visible')) {
			closeNav();
			$('#cd-shadow-layer').removeClass('is-visible');
		}
	});
	$('.nav-on-left #cd-shadow-layer').on('swipeleft', function(){
		if($('.cd-primary-nav').hasClass('nav-is-visible')) {
			closeNav();
			$('#cd-shadow-layer').removeClass('is-visible');
		}
	});
	$('#cd-shadow-layer').on('click', function(){
		closeNav();
		toggleSearch('close')
		$('#cd-shadow-layer').removeClass('is-visible');
	});


	//prevent default clicking on direct children of .cd-primary-nav 
	$('.cd-primary-nav').children('.has-children').children('a').on('click', function(event){
		event.preventDefault();
	});
	//open submenu
	$('.has-children').children('a').on('click', function(event){
		if( !checkWindowWidth() ) event.preventDefault();
		var selected = $(this);
		if( selected.next('ul').hasClass('is-hidden') ) {
			//desktop version only
			selected.addClass('selected').next('ul').removeClass('is-hidden').end().parent('.has-children').parent('ul').addClass('moves-out');
			selected.parent('.has-children').siblings('.has-children').children('ul').addClass('is-hidden').end().children('a').removeClass('selected');
			$('#cd-shadow-layer').addClass('is-visible');
		} else {
			selected.removeClass('selected').next('ul').addClass('is-hidden').end().parent('.has-children').parent('ul').removeClass('moves-out');
			$('#cd-shadow-layer').removeClass('is-visible');
		}
		toggleSearch('close');
	});

	//submenu items - go back link
	$('.go-back').on('click', function(){
		$(this).parent('ul').addClass('is-hidden').parent('.has-children').parent('ul').removeClass('moves-out');
	});

	function closeNav() {
		$('.cd-nav-trigger').removeClass('nav-is-visible');
		$('.cd-main-header').removeClass('nav-is-visible');
		$('.cd-primary-nav').removeClass('nav-is-visible');
		$('.has-children ul').addClass('is-hidden');
		$('.has-children a').removeClass('selected');
		$('.moves-out').removeClass('moves-out');
		$('.cd-main-content').removeClass('nav-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
			$('body').removeClass('overflow-hidden');
		});
	}

	function toggleSearch(type) {
		if(type=="close") {
			//close serach 
			$('.cd-search').removeClass('is-visible');
			$('.cd-search-trigger').removeClass('search-is-visible');
			$('#cd-shadow-layer').removeClass('search-is-visible');
		} else {
			//toggle search visibility
			$('.cd-search').toggleClass('is-visible');
			$('.cd-search-trigger').toggleClass('search-is-visible');
			$('#cd-shadow-layer').toggleClass('search-is-visible');
			if($(window).width() > MqL && $('.cd-search').hasClass('is-visible')) $('.cd-search').find('input[type="search"]').focus();
			($('.cd-search').hasClass('is-visible')) ? $('#cd-shadow-layer').addClass('is-visible') : $('#cd-shadow-layer').removeClass('is-visible') ;
		}
	}

	function checkWindowWidth() {
		//check window width (scrollbar included)
		var e = window, 
            a = 'inner';
        if (!('innerWidth' in window )) {
            a = 'client';
            e = document.documentElement || document.body;
        }
        if ( e[ a+'Width' ] >= MqL ) {
			return true;
		} else {
			return false;
		}
	}

	function moveNavigation(){
		var navigation = $('.cd-nav');
  		var desktop = checkWindowWidth();
        if ( desktop ) {
			navigation.detach();
			navigation.insertBefore('.cd-header-buttons');
		} else {
			navigation.detach();
			navigation.insertAfter('.cd-main-content');
		}
	}
	//Lateral Cart
	var $L = 1200,
		$menu_navigation = $('#main-nav'),
		$cart_trigger = $('.cd-cart-trigger'),
		$cart_items = $('.cd-cart-items')
		$hamburger_icon = $('#cd-hamburger-menu'),
		$lateral_cart = $('#cd-cart'),
		$shadow_layer = $('#cd-shadow-layer');

	//open lateral menu on mobile
	$hamburger_icon.on('click', function(event){
		event.preventDefault();
		//close cart panel (if it's open)
		$lateral_cart.removeClass('speed-in');
		toggle_panel_visibility($menu_navigation, $shadow_layer, $('body'));
	});

	//open cart
	$cart_trigger.on('click', function(event){
		event.preventDefault();
		//close lateral menu (if it's open)
		$menu_navigation.removeClass('speed-in');	
		toggle_panel_visibility($lateral_cart, $shadow_layer, $('body'));
	});

	//close lateral cart or lateral menu
	$shadow_layer.on('click', function(){
		$shadow_layer.removeClass('is-visible');
		// firefox transitions break when parent overflow is changed, so we need to wait for the end of the trasition to give the body an overflow hidden
		if( $lateral_cart.hasClass('speed-in') ) {
			$lateral_cart.removeClass('speed-in').on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
			$('body').removeClass('overflow-hidden');
			});
			$('body').addClass('overflow-auto');
			$('body').removeAttr('style');
			$menu_navigation.removeClass('speed-in');
		} else {
			$menu_navigation.removeClass('speed-in').on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
			$('body').removeClass('overflow-hidden');
			});
			$lateral_cart.removeClass('speed-in');
		}
	});
	// Get Scrollbar Width
	function getScrollbarWidth() {
	  	var outer = document.createElement("div");
	  	outer.style.visibility = "hidden";
	  	outer.style.width = "100px";
	  	outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps

	  	document.body.appendChild(outer);

	  	var widthNoScroll = outer.offsetWidth;
	  	// force scrollbars
	  	outer.style.overflow = "scroll";

	  	// add innerdiv
	  	var inner = document.createElement("div");
	  	inner.style.width = "100%";
	  	outer.appendChild(inner);        

	  	var widthWithScroll = inner.offsetWidth;

	  	// remove divs
	  	outer.parentNode.removeChild(outer);
	  	return widthNoScroll - widthWithScroll;
	}

	function toggle_panel_visibility ($lateral_cart, $background_layer, $body, $cart_items) {
		if( $lateral_cart.hasClass('speed-in') ) {
				$lateral_cart.removeClass('speed-in').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
				$body.removeClass('overflow-hidden');
				$lateral_cart.removeAttr('style');
				$('.cd-cart-items-wrap').removeAttr('style');
				$('.cd-cart-header').removeAttr('style');
				$('.cd-cart-items-inner').removeAttr('style');
				$('.cd-cart-footer').removeAttr('style');
				$('.navbar').removeAttr('style');
				$body.removeAttr('style');
			});
			$background_layer.removeClass('is-visible');
			$body.removeClass('overflow-auto');

		} else {
			$lateral_cart.addClass('speed-in').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
				$body.removeClass('overflow-auto');
				$body.addClass('overflow-hidden');
				$lateral_cart.css({width: 'calc(360px + '+ getScrollbarWidth() +'px)'});
				$('.navbar').css({width: 'calc(100% - '+ getScrollbarWidth() +'px)'});
				$body.css({width: 'calc(100% - '+ getScrollbarWidth() +'px)'});
				$('.cd-cart-items-wrap').css('overflow-y', 'scroll');
				$('.cd-cart-header').css({width: 'calc(100% - '+ getScrollbarWidth() +'px)'});
				$('.cd-cart-footer').css({width: 'calc(100% - '+ getScrollbarWidth() +'px)'});
				$('.cd-cart-items-inner').css('width', 'calc(100%)');
			});
			$('.cd-search').removeClass('is-visible');
			$('.cd-search-trigger').removeClass('search-is-visible');
			$('#cd-shadow-layer').removeClass('search-is-visible');
			$background_layer.addClass('is-visible');
			$lateral_cart.css('overflow-y', '');
			$cart_items.css('overflow-y', 'scroll')
		}
}

	 // Fave Item
      $("a.fave").click(function(faveClick) { 
        faveClick.preventDefault();     
        $(this).toggleClass("active");
      });
});
